(* ===== Unit Analysis =====
     By: Igor Nunes, aka thoga31

Routines for pausing the application.
*)

{$mode objfpc}
unit UNavigation;

interface
uses crt;

TYPE TKeys = set of char;

procedure Pause(const PauseText : string; const KeysToProceed : TKeys; out KeyReceiver : char);
procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;
procedure Pause(const PauseText : string); overload;
procedure Pause; overload;


implementation

procedure Pause(const PauseText : string; const KeysToProceed : TKeys; out KeyReceiver : char);

(* PROCEDIMENTO COMPLETO - recebe texto a mostrar, teclas que desbloqueiam a pausa e a variável receptora da chave *)
begin
   write(PauseText);
   repeat
      KeyReceiver := ReadKey;
   until KeyReceiver in KeysToProceed;
end;


procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;

(* PROCEDIMENTO COMPACTO - não há output *)
var Key : char;
begin
   Pause(PauseText, KeysToProceed, Key);
end;


procedure Pause(const PauseText : string); overload;

(* PROCEDIMENTO SIMPLIFICADO - assume Enter como tecla a processar *)
begin
   Pause(PauseText, [#13]);
end;


procedure Pause; overload;

const StrEmpty : string = '';
begin
   Pause(StrEmpty);
end;

end.