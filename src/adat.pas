(* ===== ADAT - Age Distributon Analysis Tool =====
Version: 1.2.0
     By: Igor Nunes, aka thoga31

Based on the software "fael - Ferramenta de Apoio ao Estudo da Lista" developed by A. Pedro Cunha.
*)

{$mode objfpc}
{$ifdef windows} {$unitpath /wingraph} {$endif}
program adat_120;
uses crt, classes, sysutils, strutils, math, {$ifdef windows} wingraph, windows, {$endif}
     UNavigation;  // It just has 4 overloads of the procedure "Pause".

const ID_FAMILY_MIN = 1;
      ID_FAMILY_MAX = 99999;
      NEWLINE = {$ifdef windows} #13+#10 {$else} #10 {$endif};
      PYRAMID_DEFAULT_STEP         = 5;
      PYRAMID_DEFAULT_MIN          = 0;
      PYRAMID_DEFAULT_MAX          = 75;
      PYRAMID_DEFAULT_COLUMN_WIDTH = 15;
      AGE_MIN = 0;
      AGE_MAX = 200;  // hypotetic, just for computation
      FILENAME_DEFAULT = 'data.txt';

type TGender = (GENDER_Male, GENDER_Female, GENDER_Undefined);
     TAge    = AGE_MIN .. AGE_MAX;
     TSetOfAge = record
                    min, max : TAge;
                 end;
     TFamilyIndex     = array [ID_FAMILY_MIN .. ID_FAMILY_MAX] of longword;
     TArrayOfGender   = array [TGender] of longword;
     TPyramid         = array [TAge] of TArrayOfGender;
     TArrayOfSetOfAge = array of TSetOfAge;

const PERSON_YOUTH  : TSetOfAge = (min:AGE_MIN; max:14     );
      PERSON_ACTIVE : TSetOfAge = (min:15;      max:64     );
      PERSON_OLD    : TSetOfAge = (min:65;      max:AGE_MAX);


function Char2Gender(const ch : char) : TGender;
(* Converts a character into a TGender. *)
begin
   case UpCase(ch) of
      'M' : Char2Gender := GENDER_Male;
      'F' : Char2Gender := GENDER_Female;
   else
      Char2Gender := GENDER_Undefined;  // This must be an input mistake.
   end;
end;


procedure InitializeArrayOfLW(out arr : array of longword);
(* Initializes any array of longword with 0s. *)
var i : longint;
begin
   for i := Low(arr) to High(arr) do
      arr[i] := 0;
end;


procedure InitializePyramid(out p : TPyramid);
(* Initializes any pyramid with 0s. *)
var a : TAge;
    g : TGender;
begin
   for a := AGE_MIN to AGE_MAX do
      for g := GENDER_Male to GENDER_Undefined do
         P[a][g] := 0;
end;


function CountFamilies(const arr : array of longword) : longword;
(* Counts the number of families based on the Family ID. *)
var i : longint;
begin
   CountFamilies := 0;
   for i := Low(arr) to High(arr) do
      Inc(CountFamilies, Byte(arr[i] > 0));
end;


function PyrCount(const pyr : TPyramid; const setage : TSetOfAge; const g : TGender) : longword;
(* Given the pyramid "pyr", it counts, for the "g" gender, the total of people within "setage". *)
var a : TAge;
begin
   PyrCount := 0;
   for a := setage.min to setage.max do
      Inc(PyrCount, pyr[a][g]);
end;


function FormatSet(const s : TSetOfAge; const greatest_age : longword = AGE_MAX) : string;
(* Output formatting - [mm,MM] - e.g., [ 7,12], [ 1,4 ], [60,..]. *)
begin
   FormatSet := '[' + PadLeft(IntToStr(s.min), 2) + ',' + IfThen(s.max>=greatest_age, '..', PadRight(IntToStr(s.max), 2)) + ']';
end;


function BuildArrayOfSetOfAge(min, max, step : longword; include_infinite : boolean = false) : TArrayOfSetOfAge;
(* Returns an array of TSetOfAge starting with min and ending with max, step by step, and it may include infinite.
e.g., min = 5, max = 15, step = 5, include_infinite = true -> [[5,9], [10,14], [15,..]] *)
var total : longword;
    i     : longint;
begin
   total := (max - min) div step + Byte(include_infinite);
   SetLength(BuildArrayOfSetOfAge, total);
   for i := Low(BuildArrayOfSetOfAge) to High(BuildArrayOfSetOfAge) do begin
      if min >= max then begin
         BuildArrayOfSetOfAge[i].min := min;
         BuildArrayOfSetOfAge[i].max := AGE_MAX;
      end else begin
         BuildArrayOfSetOfAge[i].min := min;
         BuildArrayOfSetOfAge[i].max := min+step-1;
      end;
      Inc(min, step);
   end;
end;


procedure DrawPyramid(const pyr : TPyramid;
                      const setages : array of TSetOfAge;
                      {$ifdef windows}
                         const render_visual : boolean = false;
                         const visual_dist : longword = 0;
                      {$endif}
                      const column_width : word = PYRAMID_DEFAULT_COLUMN_WIDTH);
(* Renders the pyramid for the set of ages defined. *)

var male, female : array of longint;
    maxval : longword = 0;


   {$ifdef windows}
   procedure RenderVisualPyramid(const DISTFROMINIT : longword = 0);
   (* Draws the pyramid in Graph Mode. *)
   const INIT_X = 5;
         INIT_Y = 5;
         DIFF = 0;
         SCALE  = 15;
         WIDTH  = 150;
         SPACER = 5;
   
   var i : longint;
       x1, x2 : smallint;
   
   begin
      for i := High(setages) downto Low(setages) do begin
         // Male column
         x2 := INIT_X + SPACER + TextWidth(IntToStr(maxval)) + WIDTH;
         x1 := x2 - (male[i] * WIDTH div maxval);
         OutTextXY(INIT_X, INIT_Y + ((High(setages)-i+1)*SCALE) + DISTFROMINIT, PadLeft(IntToStr(male[i]), Length(IntToStr(maxval))));
         SetFillStyle(SolidFill, CYAN);
         Bar(x1, INIT_Y + ((High(setages)-i+1)*SCALE) - DIFF + DISTFROMINIT, x2, INIT_Y + ((High(setages)-i+1)*SCALE) + SCALE - DIFF + DISTFROMINIT);
         wingraph.Rectangle(x1, INIT_Y + ((High(setages)-i+1)*SCALE) - DIFF + DISTFROMINIT, x2, INIT_Y + ((High(setages)-i+1)*SCALE) + SCALE - DIFF + DISTFROMINIT);
         
         // Set text
         OutTextXY(INIT_X + WIDTH + TextWidth(IntToStr(maxval)) + 2*SPACER, INIT_Y + ((High(setages)-i+1)*SCALE) + DISTFROMINIT, FormatSet(setages[i]));
         
         // Female column
         x1 := INIT_X + 3*SPACER + TextWidth(IntToStr(maxval)) + TextWidth(FormatSet(setages[i])) + WIDTH;
         x2 := x1 + (female[i] * WIDTH div maxval);
         OutTextXY(INIT_X + 2*WIDTH + TextWidth(IntToStr(maxval)) + TextWidth(FormatSet(setages[i])) + 4*SPACER, INIT_Y + ((High(setages)-i+1)*SCALE) + DISTFROMINIT, PadLeft(IntToStr(female[i]), Length(IntToStr(maxval))));
         SetFillStyle(SolidFill, MAGENTA);
         Bar(x1, INIT_Y + ((High(setages)-i+1)*SCALE) - DIFF + DISTFROMINIT, x2, INIT_Y + ((High(setages)-i+1)*SCALE) + SCALE - DIFF + DISTFROMINIT);
         wingraph.Rectangle(x1, INIT_Y + ((High(setages)-i+1)*SCALE) - DIFF + DISTFROMINIT, x2, INIT_Y + ((High(setages)-i+1)*SCALE) + SCALE - DIFF + DISTFROMINIT);
      end;
   end;
   {$endif}


procedure DrawPyramid(const pyr : TPyramid;
                      const setages : array of TSetOfAge;
                      const column_width : word = PYRAMID_DEFAULT_COLUMN_WIDTH); overload;
(* Simpler version without the visual-related parameters. *)
begin
   DrawPyramid(pyr, setages, false, 0, column_width);
end;


var i : longint;

begin
   SetLength(male,   Length(setages));
   SetLength(female, Length(setages));
   
   for i := Low(setages) to High(setages) do begin  // Gathers data and determines the maximum count.
      male[i]   := PyrCount(pyr, setages[i], GENDER_Male);
      female[i] := PyrCount(pyr, setages[i], GENDER_Female);
      maxval := Max(male[i], Max(maxval, female[i]));
   end;
   
   {$ifdef windows}
   if render_visual then
      RenderVisualPyramid(visual_dist)
   else {$endif}
   for i := High(setages) downto Low(setages) do begin  // Renders the pyramid.
      writeln('  ', PadLeft(IntToStr(male[i]), Length(IntToStr(maxval))), '  ', PadLeft(DupeString('#', (male[i]*column_width) div maxval), column_width),
              ' ', FormatSet(setages[i]), ' ',
              PadRight(DupeString('#', (female[i]*column_width) div maxval), column_width), '  ', PadLeft(IntToStr(female[i]), Length(IntToStr(maxval))));
   end;
end;


var dataname  : string;
    datafile  : TStringList;
    familyID  : TFamilyIndex;
    pyramid   : TPyramid;
    sumperson : longword = 0;
    sumgender : TArrayOfGender = (0, 0, 0);
    sumyouth  : longword = 0;
    sumold    : longword = 0;
    sumactive : longword = 0;


procedure ProcessData;
(* Reads the contents of the file and processes the data contained in it. *)
var person : string;
    gender : char;
    id     : longword;
    age    : word;

begin
   for person in datafile do begin
      if SScanF(person, '%d%c%d', [@id, @gender, @age]) = 3 then begin
         
         Inc(familyID[id div 100]);
         Inc(pyramid[age][Char2Gender(gender)]);
         Inc(sumgender[Char2Gender(gender)]);
         Inc(sumperson);
         
         case age of  // process on-the-fly for big-groups data
             0.. 14 : Inc(sumyouth);
            15.. 64 : Inc(sumactive);
            65..200 : Inc(sumold);
         end;
      end;
   end;
end;


procedure ShowData;
(* Ouputs the results to the output stream. *)
var sumfamily : longword;
    {$ifdef windows} driver, modus : smallint; {$endif}
    opt : char;
begin
   writeln('Populacao residente: ', sumperson);
   writeln(' ', double(sumgender[GENDER_Male])   / double(sumperson) * 100.0 :5:1, '% homens (',   sumgender[GENDER_Male],   ')');
   writeln(' ', double(sumgender[GENDER_Female]) / double(sumperson) * 100.0 :5:1, '% mulheres (', sumgender[GENDER_Female], ')');
   
   writeln;
   
   sumfamily := CountFamilies(familyID);
   writeln(sumfamily, ' familias, com ', sumperson / sumfamily :0:1, ' pessoas por familia (media).');
   
   writeln;
   
   writeln('Piramide etaria segundo os grandes grupos etarios:');
   DrawPyramid(pyramid, [PERSON_YOUTH, PERSON_ACTIVE, PERSON_OLD]);
   
   writeln;
   
   writeln('Indice de Envelhecimento: ', sumold / sumyouth * 100 :0:1, '%');
   
   writeln;
   
   writeln('Jovens:  ', sumyouth  / sumperson * 100 :4:1, '%');
   writeln('Activos: ', sumactive / sumperson * 100 :4:1, '%');
   writeln('Idosos:  ', sumold    / sumperson * 100 :4:1, '%');
   
   writeln;
   
   writeln('Indices de Dependencia:');
   writeln('  Total:  ', (sumyouth + sumold) / sumactive * 100 :4:1, '%');
   writeln('  Jovens: ', sumyouth            / sumactive * 100 :4:1, '%');
   writeln('  Idosos: ', sumold              / sumactive * 100 :4:1, '%');
   
   writeln;
   
   {$ifdef windows}
   Pause('Pressione ENTER para ver piramide etaria...');
   writeln;
   {$endif}
   
   DrawPyramid(pyramid, BuildArrayOfSetOfAge(PYRAMID_DEFAULT_MIN, PYRAMID_DEFAULT_MAX, PYRAMID_DEFAULT_STEP, true));
   
   // Only for Windows: optional Graph Mode
   {$ifdef windows}
   writeln;
   Pause('Deseja ver as piramides em modo grafico? (S/N) ', ['n', 's', 'N', 'S'], opt);
   writeln;
   if UpCase(opt) = 'S' then
      try
         try
            DetectGraph(driver, modus);
            modus := mCustom;
            SetWindowSize(600,500);
            InitGraph(driver, modus, 'ADAT 1.2.0 - Piramides etarias');
            DrawPyramid(pyramid, [PERSON_YOUTH, PERSON_ACTIVE, PERSON_OLD], true);
            DrawPyramid(pyramid, BuildArrayOfSetOfAge(PYRAMID_DEFAULT_MIN, PYRAMID_DEFAULT_MAX, PYRAMID_DEFAULT_STEP, true), true, 100);
         except
            on ex : Exception do
               writeln(NEWLINE, 'ERROR: ', ex.classname, ' -> ', ex.message);
         end;
      finally
         Pause('Press Enter to exit graph mode.');
         CloseGraph;
      end;
   {$endif}
end;


begin  (* Main block *)
   if (ParamCount = 1) or ((ParamCount = 0) and FileExists(FILENAME_DEFAULT)) then begin
   // If the default file doesn't exist, the file must be given as the only parameter.
      SetConsoleTitle('ADAT - Age Distribution Analysis Tool (1.2.0)');
      writeln('ADAT - Age Distribution Analysis Tool, version 1.2.0, by Igor Nunes');
      writeln;
      
      try
         dataname := IfThen(ParamCount = 0, FILENAME_DEFAULT, ParamStr(1));
         datafile := TStringList.Create;
         
         try
            // Initialization
            InitializeArrayOfLW(familyID);
            InitializePyramid(pyramid);
            
            // Loading data
            datafile.LoadFromFile(dataname);
            
            ProcessData;  // Processing data
            ShowData;     // Outputing results
         
         except
            on ex : Exception do
               writeln(NEWLINE, 'ERROR: ', ex.classname, ' -> ', ex.message);
         end;
      
      finally
         datafile.Destroy;
      end;
   
   end else if ParamCount = 0 then  // The default file doesn't exist.
      writeln('ABORTED: No given file.')
   else  // Any other case
      writeln('ABORTED: Too much parameters.');
   
   {$ifdef windows}
   writeln;
   Pause('Done.');
   {$endif}
end.