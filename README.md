ADAT, Age Distribution Analysis Tool

1.2.0

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This application makes an analysis of a file, 'data.txt', which contains data about a certain population.

Each line in the file refers to a person and has the format "id_gender_age" ("_" represents empty).
   The ID has the number of the family followed by a 2 digit number that represents the member.
   E.g.: 1234501m34 is a 34-year-old male and is the member nº 01 of family nº 12345.
